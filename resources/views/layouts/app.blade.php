<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Simulasi Un</title>
	<link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets/images/icon.png')}}" />
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/css/simulasi.css')}}">
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-4">
                <div class="login">
                    <div class="form">
                        @yield('content')
                    </div>
                </div>
            </div>
            <div class="col-8">
                <img src="{{asset('assets/images/login/login.JPG')}}" class="img-fluid bgLogin" alt="Responsive image">
            </div>
        </div>
    </div>		  
</body>
</html>
