<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pertanyaan extends Migration{
   
    public function up(){
        Schema::create('pertanyaans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('idMapel');
            $table->string('pertanyaan')->nullable();
            $table->integer('idPilgan');
            $table->string('kunciJawaban')->nullable();
            $table->timestamps();
        });
    }


    public function down(){
        Schema::dropIfExists('pertanyaans');
    }
}
