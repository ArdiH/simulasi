<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Mapel;
use App\Model\kunciJawaban;
use App\Model\Pertanyaan;
use App\Model\pilihanGanda;
use Carbon\Carbon;

class soalController extends Controller{
  
    public function index(){
        $soal = Mapel::all();
        return view('admin.soal.index', compact('soal'));
    }

    public function create(){
       
    }

    public function store(Request $request){
        
    }

    public function show($id){
        
    }

    public function edit($id){
        $pertanyaan = Pertanyaan::where('idMapel', $id)->first();
        $pertanyaan2 = Pertanyaan::where('idMapel', $id)->get();
        return view('admin.soal.pertanyaanIndex', compact('pertanyaan','pertanyaan2'));
    }

    public function update(Request $request, $id){
       

        for($i=0;$i<count($request->pilgan1);$i++){
            $pilgan = new pilihanGanda();
            $pilgan->A = $request->pilgan1[$i];
            $pilgan->B = $request->pilgan2[$i];
            $pilgan->C = $request->pilgan3[$i];
            $pilgan->D = $request->pilgan4[$i];
            $pilgan->save();

            // pertanyaan
            $pertanyaan = Pertanyaan::where('idMapel', $id)->get();
            $pertanyaan[$i]->idPilgan = $pilgan->id;
            $pertanyaan[$i]->pertanyaan = $request->soal[$i];
            $pertanyaan[$i]->kunciJawaban = $request->kunciJawaban[$i];
            $pertanyaan[$i]->save();
                
        }


  

       


        
        //kunci 
        // $kunci = kunciJawaban::where('id', $id)->first();
        // $kunci->kunci1 = $request->kunci1;
        // $kunci->kunci2 = $request->kunci2;
        // $kunci->save();

        return redirect(route('soal.index'));

    }

    public function destroy($id){
        
    }
}
