<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Request;
use Hash;

class User extends Authenticatable{
    use Notifiable, HasRoles;
    
    const ROLE_ADMIN = 1;
    const ROLE_SISWA = 2;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at', 'email_verified_at',
    ];

    protected static function boot(){
        parent::boot();
        // When saving a record and there is a password in the request, encrypt the password
        static::saving(function ($user) {
            // If the request has a password field, we've submitted a form...
            if (request()->has('password')) {
                $user->password = $user->getOriginal('password');
                // If the field is filled, user is requesting to set a new password
                if (request()->filled('password')) {
                    $user->password = Hash::make(request('password'));
                }
            }
        });
    }

    public function roles(){
        return $this->belongsToMany(Role::class);
    }

    public function putRole($role){
        if (is_string($role)){
            $role = Role::whereRoleName($role)->first();
        }
        return $this->roles()->attach($role);
    }

    public function forgetRole($role){
        if (is_string($role))
        {
            $role = Role::whereRoleName($role)->first();
        }
        return $this->roles()->detach($role);
    }
    public function hasRole($roleName){
        foreach ($this->roles as $role)
        {
            if ($role->name === $roleName) return true;
        }
            return false;
    }
}
